import OutputAdapterFactory from '../../src/data/adapters/OutputAdapterFactory';
import { MOCK_INVALID_READINGS, MOCK_VALID_READINGS } from './test-files/test-data';
import { IOutputAdapter } from '../../src/data/adapters/IOutputAdapter';
import ConsoleAdapter from '../../src/data/adapters/ConsoleAdapter';

const ADAPTER_KEY = 'ConsoleAdapter';

describe('ConsoleAdapter', () => {
  let logSpy: jest.SpyInstance,
    warnSpy: jest.SpyInstance,
    errSpy: jest.SpyInstance,
    adapter: IOutputAdapter;

  beforeAll(() => {
    adapter = OutputAdapterFactory.create(ADAPTER_KEY);
  });

  beforeEach(() => {
    logSpy = jest.spyOn(console, 'log').mockImplementation();
    warnSpy = jest.spyOn(console, 'warn').mockImplementation();
    errSpy = jest.spyOn(console, 'error').mockImplementation();
  });

  afterEach(() => {
    logSpy.mockRestore();
    warnSpy.mockRestore();
    errSpy.mockRestore();
  });

  it('should be created by the OutputAdapterFactory', () => {
    expect(adapter).toBeDefined();
    expect(adapter.name).toEqual('Console Adapter');
    expect(adapter).toBeInstanceOf(ConsoleAdapter);
  });

  it('should output valid results to the console', async () => {
    expect(adapter).toBeDefined();

    const result = await adapter.save(MOCK_VALID_READINGS);
    expect(logSpy).toHaveBeenCalledTimes(6);
    expect(warnSpy).toHaveBeenCalledTimes(0);
    expect(errSpy).toHaveBeenCalledTimes(0);
    expect(result).toBeTruthy();

    expect(logSpy.mock.calls).toEqual([
      ['Time: 2019-12-10T00:00:00.000Z'], ['Mock Sensor 1: T=20.0, H=50.0'], [],
      ['Time: 2019-12-10T00:00:00.000Z'], ['Mock Sensor 2: T=18.2, H=45.4'], [],
    ]);
  });

  it('should output invalid results to the console without failing', async () => {
    expect(adapter).toBeDefined();

    const result = await adapter.save(MOCK_INVALID_READINGS);
    expect(logSpy).toHaveBeenCalledTimes(4);
    expect(warnSpy).toHaveBeenCalledTimes(2);
    expect(errSpy).toHaveBeenCalledTimes(0);
    expect(result).toBeTruthy();

    expect(logSpy.mock.calls).toEqual([
      ['Time: 2019-12-10T00:00:00.000Z'], [],
      ['Time: 2019-12-10T00:00:00.000Z'], [],
    ]);
  });
});
