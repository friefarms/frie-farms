import { ISensorReading } from '../../../src/sensors/ISensorReading';
import { DhtSensorReading } from '../../../src/sensors/SensorTypes';

export const MOCK_VALID_READINGS: DhtSensorReading[] = [
  {
    name: 'Mock Sensor 1',
    id: '0',
    timestamp: '2019-12-10T00:00:00.000Z',
    type: 99,
    humidity: 50,
    temperature: 20,
  },
  {
    name: 'Mock Sensor 2',
    id: '1',
    timestamp: '2019-12-10T00:00:00.000Z',
    type: 99,
    humidity: 45.36453,
    temperature: 18.22323,
  },
];

export const MOCK_INVALID_READINGS: ISensorReading[] = [
  {
    name: 'Mock Sensor 1',
    id: '0',
    timestamp: '2019-12-10T00:00:00.000Z',
    type: 99,
  },
  {
    name: 'Mock Sensor 2',
    id: '1',
    timestamp: '2019-12-10T00:00:00.000Z',
    type: 99,
  },
];
