import { IOutputAdapter } from '../../src/data/adapters/IOutputAdapter';
import OutputAdapterFactory from '../../src/data/adapters/OutputAdapterFactory';
import UnknownAdapter from '../../src/data/adapters/UnknownAdapter';
import { MOCK_INVALID_READINGS, MOCK_VALID_READINGS } from './test-files/test-data';

const ADAPTER_KEY = 'AzureAdapter';

describe('UnknownAdapter', () => {
  let adapter: IOutputAdapter;

  beforeAll(() => {
    adapter = OutputAdapterFactory.create(ADAPTER_KEY);
  });

  it('should be created by the OutputAdapterFactory', () => {
    expect(adapter).toBeDefined();
    expect(adapter.name).toEqual(`Unknown Adapter ['${ADAPTER_KEY}']`);
    expect(adapter).toBeInstanceOf(UnknownAdapter);
  });

  it('should fail calls when given valid results', async () => {
    expect(adapter).toBeDefined();

    const result = await adapter.save(MOCK_VALID_READINGS);
    expect(result).toBeFalsy();
  });

  it('should fail calls when given invalid results', async () => {
    expect(adapter).toBeDefined();

    const result = await adapter.save(MOCK_INVALID_READINGS);
    expect(result).toBeFalsy();
  });
});
