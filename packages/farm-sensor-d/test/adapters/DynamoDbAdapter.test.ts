import OutputAdapterFactory from '../../src/data/adapters/OutputAdapterFactory';
import DynamoDbAdapter from '../../src/data/adapters/DynamoDbAdapter';
import { IOutputAdapter } from '../../src/data/adapters/IOutputAdapter';
import { DynamoDB } from 'aws-sdk';
import { MOCK_INVALID_READINGS, MOCK_VALID_READINGS } from './test-files/test-data';

const ADAPTER_KEY = 'DynamoDbAdapter';
let client: DynamoDB.DocumentClient;

describe('DynamoDbAdapter', () => {
  let adapter: IOutputAdapter,
    saveSpy: jest.SpyInstance;

  beforeAll(() => {
    adapter = OutputAdapterFactory.create(ADAPTER_KEY);
    client = (adapter as DynamoDbAdapter).client;
  });

  beforeEach(() => {
    saveSpy = jest.spyOn(client, 'put').mockImplementation();
  });

  afterEach(() => {
    saveSpy.mockRestore();
  });

  it('should be created by the OutputAdapterFactory', () => {
    expect(adapter).toBeDefined();
    expect(adapter).toBeInstanceOf(DynamoDbAdapter);
    expect(adapter.name).toEqual('DynamoDB Adapter');
  });

  it('should succeed when saving no results', async () => {
    expect(adapter).toBeDefined();
    expect(client).toBeInstanceOf(DynamoDB.DocumentClient);

    const actual = await adapter.save([]);
    expect(saveSpy).toHaveBeenCalledTimes(0);
    expect(actual).toBeTruthy();
  });

  it('should save valid results to DynamoDB', async () => {
    expect(adapter).toBeDefined();
    expect(client).toBeInstanceOf(DynamoDB.DocumentClient);

    await adapter.save(MOCK_VALID_READINGS);
    expect(saveSpy).toHaveBeenCalledTimes(2);
    // expect(actual).toBeTruthy();
  });

  it('should ignore invalid results failing', async () => {
    expect(adapter).toBeDefined();
    expect(client).toBeInstanceOf(DynamoDB.DocumentClient);

    const actual = await adapter.save(MOCK_INVALID_READINGS);
    expect(saveSpy).toHaveBeenCalledTimes(0);
    expect(actual).toBeFalsy();
  });
});
