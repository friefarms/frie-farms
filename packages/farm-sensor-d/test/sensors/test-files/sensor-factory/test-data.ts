import { ISensorConfiguration } from '../../../../src/config/IConfigurationData';

export const MOCK_DHT11_CONF: ISensorConfiguration = {
  pin: 0,
  name: 'Mock DHT11 Sensor',
  errors: [],
  id: 0,
  valid: true,
  type: 11,
};

export const MOCK_DHT22_CONF: ISensorConfiguration = {
  pin: 0,
  name: 'Mock DHT22 Sensor',
  errors: [],
  id: 0,
  valid: true,
  type: 22,
};

export const MOCK_UNK_CONF: ISensorConfiguration = {
  pin: 0,
  name: 'Mock Unknown Sensor',
  errors: [],
  id: 0,
  valid: true,
  type: -1,
};
