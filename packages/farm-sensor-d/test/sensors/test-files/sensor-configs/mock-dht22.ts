import { ISensorConfiguration } from '../../../../src/config/IConfigurationData';

export const MOCK_DHT22_VALID_CONF: ISensorConfiguration = {
  id: 0,
  name: 'Mock DHT22 Sensor',
  pin: 0,
  type: 22,
  valid: true,
  errors: [],
};

export const MOCK_DHT22_INVALID_CONF: ISensorConfiguration = {
  id: 0,
  name: 'Mock DHT22 Sensor',
  pin: -1,
  type: 22,
  valid: false,
  errors: ['Invalid pin specified'],
};

export const MOCK_DHT22_READ_RESULTS = {
  temperature: 20.223223,
  humidity: 45.22233,
};
