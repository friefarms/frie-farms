import { ISensorConfiguration } from '../../../../src/config/IConfigurationData';

export const SENSOR_MGR_SENSOR_CONF: ISensorConfiguration[] = [
  {
    errors: [],
    id: 0,
    name: 'Mock Sensor 1',
    pin: 0,
    type: 99,
    valid: true,
  },
  {
    errors: [],
    id: 1,
    name: 'Mock Sensor 2',
    pin: 0,
    type: 99,
    valid: true,
  },
];
