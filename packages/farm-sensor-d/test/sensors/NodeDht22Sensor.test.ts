// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
import { promises as nodeDht } from 'node-dht-sensor';
import NodeDht22Sensor from '../../src/sensors/NodeDht22Sensor';
import SensorFactory from '../../src/sensors/SensorFactory';
import {
  MOCK_DHT22_INVALID_CONF,
  MOCK_DHT22_READ_RESULTS,
  MOCK_DHT22_VALID_CONF,
} from './test-files/sensor-configs/mock-dht22';
import { DhtSensorReading } from '../../src/sensors/SensorTypes';

describe('NodeDht22Sensor', () => {
  let sensorSpy: jest.SpyInstance;

  beforeEach(() => {
    sensorSpy = jest.spyOn(nodeDht, 'read').mockReturnValue(MOCK_DHT22_READ_RESULTS);
  });

  afterEach(() => {
    sensorSpy.mockRestore();
  });

  it('should be crated from the SensorFactory', () => {
    const sensor = SensorFactory.create(MOCK_DHT22_VALID_CONF);
    expect(sensor).toBeDefined();
    expect(sensor).toBeInstanceOf(NodeDht22Sensor);
  });

  it('should read from valid sensor', async () => {
    const sensor = SensorFactory.create(MOCK_DHT22_VALID_CONF);
    expect(sensor).toBeDefined();

    const reading = await sensor.read();
    expect(reading.type).toEqual(22);
    expect(reading.id).toEqual('0');
    expect(reading.name).toEqual('Mock DHT22 Sensor');

    const dhtReading = reading as DhtSensorReading;
    expect(dhtReading.humidity).toEqual(45.22233);
    expect(dhtReading.temperature).toEqual(20.223223);
  });

  it('should read from invalid sensor', async () => {
    const sensor = SensorFactory.create(MOCK_DHT22_INVALID_CONF);
    expect(sensor).toBeDefined();

    const reading = await sensor.read();
    expect(reading.type).toEqual(22);
    expect(reading.id).toEqual('0');
    expect(reading.name).toEqual('Mock DHT22 Sensor');

    const dhtReading = reading as DhtSensorReading;
    expect(dhtReading.humidity).toEqual(0);
    expect(dhtReading.temperature).toEqual(0);
  });
});
