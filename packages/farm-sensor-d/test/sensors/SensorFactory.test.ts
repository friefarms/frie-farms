import SensorFactory from '../../src/sensors/SensorFactory';
import NodeDht22Sensor from '../../src/sensors/NodeDht22Sensor';
import { MOCK_DHT22_CONF, MOCK_UNK_CONF } from './test-files/sensor-factory/test-data';
import UnknownSensor from '../../src/sensors/UnknownSensor';

describe('SensorFactory', () => {
  it('should create DHT 22 sensors types', () => {
    const dht22 = SensorFactory.create(MOCK_DHT22_CONF);
    expect(dht22).toBeDefined();
    expect(dht22).toBeInstanceOf(NodeDht22Sensor);
  });

  it('should not fail when building unknown types of sensors', () => {
    const unk = SensorFactory.create(MOCK_UNK_CONF);
    expect(unk).toBeDefined();
    expect(unk).toBeInstanceOf(UnknownSensor);
  });
});
