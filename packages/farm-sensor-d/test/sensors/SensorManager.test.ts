import NullReading from '../../src/sensors/NullReading';
import SensorManager from '../../src/sensors/SensorManager';
import { SENSOR_MGR_SENSOR_CONF } from './test-files/sensor-manager/test-data';

describe('SensorManager', () => {
  it('should load from configuration', () => {
    const mgr = new SensorManager(SENSOR_MGR_SENSOR_CONF);
    expect(mgr).toBeDefined();
    expect(mgr.sensorCount).toEqual(2);
  });

  it('should read all sensors without failing', async () => {
    const mgr = new SensorManager(SENSOR_MGR_SENSOR_CONF);
    expect(mgr).toBeDefined();

    const readings = await mgr.readAllSensors();
    expect(readings.length).toEqual(2);
    expect(readings[0]).toBeInstanceOf(NullReading);
    expect(readings[1]).toBeInstanceOf(NullReading);
  });

  it('should handle calls when no sensors are configured', async () => {
    const mgr = new SensorManager([]);
    expect(mgr).toBeDefined();

    const readings = await mgr.readAllSensors();
    expect(readings).toEqual([]);
  });
});
