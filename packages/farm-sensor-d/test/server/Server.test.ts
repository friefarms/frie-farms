import { main, setConfigPath, setTimerEnabled } from '../../src/server';
import * as path from 'path';

describe('Server', () => {
  let logSpy: jest.SpyInstance,
    infoSpy: jest.SpyInstance,
    warnSpy: jest.SpyInstance;

  beforeEach(() => {
    logSpy = jest.spyOn(console, 'log').mockImplementation();
    infoSpy = jest.spyOn(console, 'info').mockImplementation();
    warnSpy = jest.spyOn(console, 'warn').mockImplementation();
    setConfigPath(path.resolve(__dirname, 'test-files/test-config.json'));
    setTimerEnabled(false);
  });

  afterEach(() => {
    logSpy.mockRestore();
    infoSpy.mockRestore();
    warnSpy.mockRestore();
  });

  it('should start without error', async () => {
    await main();
    expect(logSpy).toHaveBeenCalledTimes(8);
    expect(infoSpy).toHaveBeenCalledTimes(1);
    expect(warnSpy).toHaveBeenCalledTimes(0);
  });
});
