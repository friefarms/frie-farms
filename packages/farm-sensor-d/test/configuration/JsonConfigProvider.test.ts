import * as path from 'path';
import JsonConfigProvider from '../../src/data/providers/JsonConfigProvider';
import {
  INVALID_ADAPTER_EXPECTED,
  INVALID_ID_EXPECTED,
  INVALID_SENSOR_EXPECTED,
  INVALID_TYPE_EXPECTED,
  MULTIPLE_ERR_EXPECTED,
  NO_SENSORS_EXPECTED,
  VALID_CONFIG_EXPECTED,
} from './test-files/config-test-data';

// Test Data Setup
const directory = 'test-files';
const testFilePaths = {
  badAdapter: path.resolve(__dirname, `${directory}/test-config-bad-adapter.json`),
  badId: path.resolve(__dirname, `${directory}/test-config-bad-id.json`),
  badSensor: path.resolve(__dirname, `${directory}/test-config-bad-sensor.json`),
  badTimeout: path.resolve(__dirname, `${directory}/test-config-bad-timeout.json`),
  badType: path.resolve(__dirname, `${directory}/test-config-bad-type.json`),
  good: path.resolve(__dirname, `${directory}/test-config-good.json`),
  multipleErr: path.resolve(__dirname, `${directory}/test-config-mult-err.json`),
  noSensor: path.resolve(__dirname, `${directory}/test-config-no-sensor.json`),
  noTimeout: path.resolve(__dirname, `${directory}/test-config-no-timeout.json`),
};

describe('JSON Configuration Provider', () => {
  it('should load a valid test-files JSON file', () => {
    const provider = new JsonConfigProvider(testFilePaths.good);
    expect(provider).toBeDefined();

    const config = provider.load();
    expect(config).toEqual(VALID_CONFIG_EXPECTED);
  });

  it("doesn't allow invalid timeouts", () => {
    const provider = new JsonConfigProvider(testFilePaths.badTimeout);
    expect(provider).toBeDefined();

    const config = provider.load();
    expect(config.hasOwnProperty('timeout')).toBeTruthy();
    expect(config.timeout).toEqual(10000);
  });

  it("doesn't fail when no timeout is provided", () => {
    const provider = new JsonConfigProvider(testFilePaths.noTimeout);
    expect(provider).toBeDefined();

    const config = provider.load();
    expect(config.hasOwnProperty('timeout')).toBeTruthy();
    expect(config.timeout).toEqual(10000);
  });

  it("doesn't fail when invalid output adapters are specified", () => {
    const provider = new JsonConfigProvider(testFilePaths.badAdapter);
    expect(provider).toBeDefined();

    const config = provider.load();
    expect(config).toBeDefined();
    expect(config.hasOwnProperty('outputAdapters')).toBeTruthy();
    expect(config).toEqual(INVALID_ADAPTER_EXPECTED);
  });

  it("doesn't fail when invalid sensor information is specified", () => {
    const provider = new JsonConfigProvider(testFilePaths.badSensor);
    expect(provider).toBeDefined();

    const config = provider.load();
    expect(config.hasOwnProperty('sensors')).toBeTruthy();
    expect(config.sensors.length).toEqual(2);
    expect(config).toEqual(INVALID_SENSOR_EXPECTED);
  });

  it("doesn't fail when no sensors are specified", () => {
    const provider = new JsonConfigProvider(testFilePaths.noSensor);
    expect(provider).toBeDefined();

    const config = provider.load();
    expect(config.hasOwnProperty('sensors')).toBeTruthy();
    expect(config.sensors.length).toEqual(0);
    expect(config).toEqual(NO_SENSORS_EXPECTED);
  });

  it("doesn't fail when an invalid id is specified", () => {
    const provider = new JsonConfigProvider(testFilePaths.badId);
    expect(provider).toBeDefined();

    const config = provider.load();
    expect(config.hasOwnProperty('sensors')).toBeTruthy();
    expect(config.sensors.length).toEqual(2);
    expect(config).toEqual(INVALID_ID_EXPECTED);
  });

  it("doesn't fail when an invalid type is specified", () => {
    const provider = new JsonConfigProvider(testFilePaths.badType);
    expect(provider).toBeDefined();

    const config = provider.load();
    expect(config.hasOwnProperty('sensors')).toBeTruthy();
    expect(config.sensors.length).toEqual(2);
    expect(config).toEqual(INVALID_TYPE_EXPECTED);
  });

  it("doesn't fail when multiple invalid properties are specified", () => {
    const provider = new JsonConfigProvider(testFilePaths.multipleErr);
    expect(provider).toBeDefined();

    const config = provider.load();
    expect(config.hasOwnProperty('sensors')).toBeTruthy();
    expect(config.sensors.length).toEqual(2);
    expect(config).toEqual(MULTIPLE_ERR_EXPECTED);
  });
});
