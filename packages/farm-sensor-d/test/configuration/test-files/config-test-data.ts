import { IConfigurationData } from '../../../src/config/IConfigurationData';
import ConsoleAdapter from '../../../src/data/adapters/ConsoleAdapter';
import DynamoDbAdapter from '../../../src/data/adapters/DynamoDbAdapter';
import UnknownAdapter from '../../../src/data/adapters/UnknownAdapter';

export const VALID_CONFIG_EXPECTED: IConfigurationData = {
  outputAdapters: [new ConsoleAdapter(), new DynamoDbAdapter()],
  sensors: [
    {
      errors: [],
      id: 0,
      name: 'Sensor 1',
      pin: 23,
      type: 22,
      valid: true,
    },
    {
      errors: [],
      id: 1,
      name: 'Sensor 2',
      pin: 24,
      type: 22,
      valid: true,
    },
  ],
  timeout: 10000,
};

export const INVALID_ID_EXPECTED: IConfigurationData = {
  outputAdapters: [new ConsoleAdapter()],
  sensors: [
    {
      errors: ['Invalid sensor id specified'],
      id: -1,
      name: 'Sensor 1',
      pin: 23,
      type: 22,
      valid: false,
    },
    {
      errors: [],
      id: 1,
      name: 'Sensor 2',
      pin: 24,
      type: 22,
      valid: true,
    },
  ],
  timeout: 10000,
};

export const INVALID_TYPE_EXPECTED: IConfigurationData = {
  outputAdapters: [new ConsoleAdapter()],
  sensors: [
    {
      errors: [],
      id: 0,
      name: 'Sensor 1',
      pin: 23,
      type: 22,
      valid: true,
    },
    {
      errors: ['Invalid sensor type specified'],
      id: 1,
      name: 'Sensor 2',
      pin: 24,
      type: -1,
      valid: false,
    },
  ],
  timeout: 10000,
};

export const INVALID_ADAPTER_EXPECTED: IConfigurationData = {
  outputAdapters: [
    new ConsoleAdapter(),
    new DynamoDbAdapter(),
    new UnknownAdapter('AzureAdapter'),
  ],
  sensors: [
    {
      errors: [],
      id: 0,
      name: 'Sensor 1',
      pin: 23,
      type: 22,
      valid: true,
    },
    {
      errors: [],
      id: 1,
      name: 'Sensor 2',
      pin: 24,
      type: 22,
      valid: true,
    },
  ],
  timeout: 10000,
};

export const INVALID_SENSOR_EXPECTED: IConfigurationData = {
  outputAdapters: [
    new ConsoleAdapter(),
  ],
  sensors: [
    {
      errors: ['Sensor name not specified'],
      id: 0,
      name: 'Unknown Sensor',
      pin: 23,
      type: 22,
      valid: true,
    },
    {
      errors: ['Invalid pin specified'],
      id: 1,
      name: 'Sensor 2',
      pin: -1,
      type: 22,
      valid: false,
    },
  ],
  timeout: 10000,
};

export const NO_SENSORS_EXPECTED: IConfigurationData = {
  outputAdapters: [new ConsoleAdapter()],
  sensors: [],
  timeout: 10000,
};

export const MULTIPLE_ERR_EXPECTED: IConfigurationData = {
  outputAdapters: [new ConsoleAdapter()],
  sensors: [
    {
      errors: ['Invalid sensor id specified', 'Invalid sensor type specified'],
      id: -1,
      name: 'Sensor 1',
      pin: 23,
      type: -1,
      valid: false,
    },
    {
      errors: ['Invalid sensor id specified', 'Invalid sensor type specified', 'Invalid pin specified'],
      id: -1,
      name: 'Sensor 2',
      pin: -1,
      type: -1,
      valid: false,
    },
  ],
  timeout: 10000,
};
