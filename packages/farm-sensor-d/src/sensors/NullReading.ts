import { ISensorReading } from './ISensorReading';

export default class NullReading implements ISensorReading {
  public id = '-1';
  public name = 'Invalid Sensor Reading';
  public timestamp = new Date().toISOString();
  public type = -1;
}
