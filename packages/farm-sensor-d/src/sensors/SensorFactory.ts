import { ISensorConfiguration } from '../config/IConfigurationData';
import NodeDht22Sensor from './NodeDht22Sensor';
import UnknownSensor from './UnknownSensor';
import { ISensorDataProvider } from '../data/providers/ISensorDataProvider';

export default class SensorFactory {
  public static create(config: ISensorConfiguration): ISensorDataProvider {
    switch (config.type) {
      case 22:
        return new NodeDht22Sensor(config);

      default:
        return new UnknownSensor();
    }
  }
}
