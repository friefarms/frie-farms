// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
import { promises as sensor } from 'node-dht-sensor';
import { ISensorConfiguration } from '../config/IConfigurationData';
import { DhtSensorReading } from './SensorTypes';
import { ISensorDataProvider } from '../data/providers/ISensorDataProvider';

export default class NodeDht22Sensor implements ISensorDataProvider {
  public readonly name: string;

  public readonly id: number;

  public readonly type: number = 22;

  public readonly pin: number;

  private readonly valid: boolean = false;

  constructor(config: ISensorConfiguration) {
    this.name = config.name;
    this.id = config.id;
    this.pin = config.pin;
    this.valid = config.valid;
  }

  public async read(): Promise<DhtSensorReading> {
    const value: DhtSensorReading = {
      humidity: 0,
      id: this.id.toString(),
      name: this.name,
      temperature: 0,
      timestamp: new Date().toISOString(),
      type: this.type,
    };

    if (!this.valid) { return value; }

    // perform the read
    const { temperature, humidity } = await sensor.read(this.type, this.pin);
    value.temperature = temperature;
    value.humidity = humidity;
    return value;
  }
}
