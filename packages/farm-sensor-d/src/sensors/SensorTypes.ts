import { ISensorReading } from './ISensorReading';

export type DhtSensorReading = ISensorReading & {
  temperature: number;
  humidity: number;
};
