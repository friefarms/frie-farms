export interface ISensorReading {
  id: string;
  name: string;
  type: number;
  timestamp: string;
}
