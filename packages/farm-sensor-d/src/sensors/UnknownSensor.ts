import NullReading from './NullReading';
import { ISensorDataProvider } from '../data/providers/ISensorDataProvider';
import { ISensorReading } from './ISensorReading';

export default class UnknownSensor implements ISensorDataProvider {
  public async read(): Promise<ISensorReading> {
    return new NullReading();
  }
}
