import { ISensorConfiguration } from '../config/IConfigurationData';
import SensorFactory from './SensorFactory';
import { ISensorDataProvider } from '../data/providers/ISensorDataProvider';
import { ISensorReading } from './ISensorReading';

export default class SensorManager {
  private readonly sensors: ISensorDataProvider[] = [];

  constructor(sensors: ISensorConfiguration[]) {
    this.sensors = sensors.map(s => SensorFactory.create(s));
  }

  public async readAllSensors(): Promise<ISensorReading[]> {
    return Promise.all(this.sensors.map(s => s.read()));
  }

  public get sensorCount() {
    return this.sensors.length;
  }
}
