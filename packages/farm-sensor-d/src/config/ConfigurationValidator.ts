import { ISensorConfiguration } from './IConfigurationData';

const ERR_SENSOR_NAME = 'Sensor name not specified';
const ERR_SENSOR_PIN = 'Invalid pin specified';
const ERR_SENSOR_TYPE = 'Invalid sensor type specified';
const ERR_SENSOR_ID = 'Invalid sensor id specified';
const DEFAULT_TIMEOUT = 10000;

export default class ConfigurationValidator {
  public static validateTimeout(value: number): number {
    if (!value) { return DEFAULT_TIMEOUT; }
    if (value < 0) { return DEFAULT_TIMEOUT; }

    return value;
  }

  public static validateSensorArray(sensors: any[]): ISensorConfiguration[] {
    if (sensors.length < 1) { return []; }

    return sensors.map(s => {
      const ret: ISensorConfiguration = {
        errors: [],
        id: s.hasOwnProperty('id') ? s.id : -1,
        name: s.name || 'Unknown Sensor',
        pin: s.pin || -1,
        type: s.type || -1,
        valid: true,
      };

      if (ret.name === 'Unknown Sensor') {
        ret.errors.push(ERR_SENSOR_NAME);
      }

      if (ret.id === -1) {
        ret.errors.push(ERR_SENSOR_ID);
        ret.valid = false;
      }

      if (ret.type === -1) {
        ret.errors.push(ERR_SENSOR_TYPE);
        ret.valid = false;
      }

      if (ret.pin < 0) {
        ret.errors.push(ERR_SENSOR_PIN);
        ret.valid = false;
      }

      return ret;
    });
  }
}
