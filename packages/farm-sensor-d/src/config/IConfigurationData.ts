import { IOutputAdapter } from '../data/adapters/IOutputAdapter';

export interface ISensorConfiguration {
  id: number;
  name: string;
  pin: number;
  type: number;
  valid: boolean;
  errors: string[];
}

export interface IConfigurationData {
  sensors: ISensorConfiguration[];
  timeout: number;
  outputAdapters: IOutputAdapter[];
}
