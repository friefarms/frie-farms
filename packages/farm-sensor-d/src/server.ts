import * as path from 'path';
import JsonConfigProvider from './data/providers/JsonConfigProvider';
import SensorManager from './sensors/SensorManager';
import { ISensorReading } from './sensors/ISensorReading';
import { IOutputAdapter } from './data/adapters/IOutputAdapter';

let timeBetweenChecks = 10000;
let sensorManager: SensorManager;
let storageTargets: IOutputAdapter[] = [];
let configPath = path.resolve(__dirname, 'config/sensors.json');
let timerEnabled = true;

export function setConfigPath(path: string): void {
  configPath = path;
}

export function setTimerEnabled(value: boolean): void {
  timerEnabled = value;
}

async function readSensors(): Promise<boolean[]> {
  const sensorReadings: ISensorReading[] = await sensorManager.readAllSensors();
  return Promise.all(storageTargets.map(s => s.save(sensorReadings)));
}

function configure(): void {
  const config = new JsonConfigProvider(configPath);
  const configData = config.load();

  sensorManager = new SensorManager(configData.sensors);
  storageTargets = configData.outputAdapters;
  timeBetweenChecks = configData.timeout;
}

function showConfiguration(): void {
  console.log('~~~~~| Farm Sensor Daemon |~~~~~');
  console.log('Configured Output Adapters:');
  storageTargets.map(t => {
    console.log(`  - ${t.name}`);
  });
  console.log();
  console.log(`Timeout: ${timeBetweenChecks} ms`);
  console.log(`Number of sensors: ${sensorManager.sensorCount}`);
  console.log();
  console.log();
}

async function start(): Promise<void> {
  configure();
  showConfiguration();
  readSensors();
  if (timerEnabled) {
    setInterval(readSensors, timeBetweenChecks);
  }
}

export function main(): void {
  start().then(() => {
    console.info('Monitoring started.\n\n');
  });
}
