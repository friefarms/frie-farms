import { ISensorReading } from '../../sensors/ISensorReading';

export interface ISensorDataProvider {
  read(): Promise<ISensorReading>;
}
