import { IConfigurationData } from '../../config/IConfigurationData';

export interface IConfigProvider {
  load(): IConfigurationData;
}
