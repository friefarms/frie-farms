import * as fs from 'fs';
import ConfigurationValidator from '../../config/ConfigurationValidator';
import { IConfigurationData } from '../../config/IConfigurationData';
import OutputAdapterFactory from '../adapters/OutputAdapterFactory';
import { IConfigProvider } from './IConfigProvider';
import { IOutputAdapter } from '../adapters/IOutputAdapter';

export default class JsonConfigProvider implements IConfigProvider {
  private readonly path: string;

  constructor(path: string) {
    this.path = path;
  }

  public load(): IConfigurationData {
    const rawData = fs.readFileSync(this.path);
    const configObj = JSON.parse(rawData.toString());

    // Initialize some defaults
    const config: IConfigurationData = {
      outputAdapters: [],
      sensors: [],
      timeout: 10000,
    };

    if (configObj) {
      config.outputAdapters = this.configureAdapters(configObj.outputAdapters);
      config.timeout = ConfigurationValidator.validateTimeout(configObj.timeBetweenChecks);
      config.sensors = ConfigurationValidator.validateSensorArray(configObj.sensors);
    }

    return config;
  }

  private configureAdapters(adapterNames: string[]): IOutputAdapter[] {
    return adapterNames.map(name => OutputAdapterFactory.create(name));
  }
}
