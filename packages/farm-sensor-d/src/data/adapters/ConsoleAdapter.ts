import { DhtSensorReading } from '../../sensors/SensorTypes';
import { ISensorReading } from '../../sensors/ISensorReading';
import { IOutputAdapter } from './IOutputAdapter';

export default class ConsoleAdapter implements IOutputAdapter {
  public readonly name = 'Console Adapter';

  public async save(results: ISensorReading[]): Promise<boolean> {
    results.map(sensor => {
      const s = sensor as DhtSensorReading;

      console.log(`Time: ${s.timestamp}`);
      if (s.temperature && s.humidity) {
        console.log(`${s.name}: T=${s.temperature.toFixed(1)}, H=${s.humidity.toFixed(1)}`);
      } else {
        console.warn(`${s.name} returned invalid data`);
      }
      console.log();
    });

    // Console always succeeds
    return true;
  }
}
