import { ISensorReading } from '../../sensors/ISensorReading';

export interface IOutputAdapter {
  name: string;

  save(results: ISensorReading[]): Promise<boolean>;
}
