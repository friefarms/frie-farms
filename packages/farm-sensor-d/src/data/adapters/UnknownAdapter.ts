import { IOutputAdapter } from './IOutputAdapter';
import { ISensorReading } from '../../sensors/ISensorReading';

export default class UnknownAdapter implements IOutputAdapter {
  public readonly name: string;

  constructor(name: string) {
    this.name = `Unknown Adapter ['${name}']`;
    console.error(`${this.name} specified in the config file.`);
  }

  // eslint-disable-next-line no-unused-vars
  public async save(results: ISensorReading[]): Promise<boolean> { return false; }
}
