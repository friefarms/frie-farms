import ConsoleAdapter from './ConsoleAdapter';
import DynamoDbAdapter from './DynamoDbAdapter';
import UnknownAdapter from './UnknownAdapter';
import { IOutputAdapter } from './IOutputAdapter';

export default class OutputAdapterFactory {
  public static create(name: string): IOutputAdapter {
    switch (name) {
      case 'ConsoleAdapter':
        return new ConsoleAdapter();
      case 'DynamoDbAdapter':
        return new DynamoDbAdapter();
      default:
        return new UnknownAdapter(name);
    }
  }
}
