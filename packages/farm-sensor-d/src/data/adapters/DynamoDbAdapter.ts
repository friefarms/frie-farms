import * as AWS from 'aws-sdk';
import { DynamoDB } from 'aws-sdk';
import { v4 as uuid } from 'uuid';
import { DhtSensorReading } from '../../sensors/SensorTypes';
import { ISensorReading } from '../../sensors/ISensorReading';
import { IOutputAdapter } from './IOutputAdapter';

// module-level declaration to only create one client
let documentClient: DynamoDB.DocumentClient;

export default class DynamoDbAdapter implements IOutputAdapter {
  public readonly name = 'DynamoDB Adapter';
  private readonly defaultDbParams: { TableName: string };

  public get client(): DynamoDB.DocumentClient {
    return documentClient;
  }

  constructor() {
    this.defaultDbParams = {
      TableName: 'farm-sensor-d-sensor-readings',
    };

    this.configure();
  }

  public async save(results: ISensorReading[]): Promise<boolean> {
    if (results.length === 0) { return true; }

    const promises = results.map(async sensor => {
      const s = sensor as DhtSensorReading;
      if (s.temperature && s.humidity) {
        const params = {
          ...this.defaultDbParams,
          Item: {
            humidity: s.humidity,
            id: uuid(),
            name: s.name,
            temperature: s.temperature,
            timestamp: new Date().toISOString(),
            type: s.type,
          },
        };

        documentClient.put(params, err => { return !err; });
      } else {
        return false;
      }
    });

    const saveResults = await Promise.all(promises);
    return saveResults.every(Boolean);
  }

  public configure(): void {
    if (!documentClient) {
      AWS.config.update({ region: 'us-east-2' });
      documentClient = new DynamoDB.DocumentClient({ apiVersion: '2012-08-10' });
    }
  }
}
