import gql from 'graphql-tag'

export const GET_LATEST_SENSOR_READING = gql`
    query latestReading($name: String!) {
        getLatestSensorReading(name: $name) {
            id
            humidity
            name
            temperature
            timestamp
            type
        }
    }
`
