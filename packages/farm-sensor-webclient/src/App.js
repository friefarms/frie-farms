import React from 'react'
import background from './assets/background.png'
import styled, { createGlobalStyle } from 'styled-components'
import CssBaseline from '@material-ui/core/CssBaseline'
import TemperatureSensorList from './components/SensorDisplays/TemperatureSensorList'
import { Box } from '@material-ui/core'
import { ApolloProvider } from 'react-apollo'
import AWSAppSyncClient from 'aws-appsync/lib'
import appSyncConfig from './aws-exports'

const GlobalStyle = createGlobalStyle`
  body {
    background-image: url(${background});
    background-repeat: no-repeat;
    
    width: 800px;
    max-width: 800px;
    min-width: 800px;
    
    height: 480px;
    max-height: 480px;
    min-height: 480px;
    
    overflow: hidden;
  }
`

const Header = styled.div`
  height: 135px;
`

const HeaderText = styled('span')`
  color: #f1eebe;
  padding-left: 8px;
  font-size: 48pt;
  font-weight: bold;
  line-height: 135px;
  vertical-align: middle;
  text-shadow: 0px 0px 9px #000000;  
`

const SensorArea = styled(Box)`
  width: 100%;
  margin: 0 auto;
  display: flex;
  justify-content: center;
`

const sensors = ['Sensor 1', 'Sensor 2']

const client = new AWSAppSyncClient({
  url: appSyncConfig.graphqlEndpoint,
  region: appSyncConfig.region,
  auth: {
    type: appSyncConfig.authenticationType
  },
  disableOffline: true
})

function App () {
  return (
    <ApolloProvider client={client}>
      <CssBaseline />
      <GlobalStyle />
      <Header>
        <HeaderText>
          Farm Manager
        </HeaderText>
      </Header>
      <SensorArea>
        <TemperatureSensorList sensors={sensors} />
      </SensorArea>
    </ApolloProvider>
  )
}

export default App
