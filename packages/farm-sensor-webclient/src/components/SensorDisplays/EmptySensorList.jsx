import PropTypes from 'prop-types'

import React from 'react'
import { Box } from '@material-ui/core'
import styled from 'styled-components'

const EmptySensorList = ({ className }) => {
  return (
    <Box className={className}>
      No Sensors Found
    </Box>
  )
}

EmptySensorList.propTypes = {
  className: PropTypes.string
}

export default styled(EmptySensorList)`
  color: #f1eebe;
  text-shadow: 0px 0px 9px #333333;
  text-align: center;
  line-height: 315px;  
  font-size: 36pt;
`
