import PropTypes from 'prop-types'
import React, { useEffect, useState } from 'react'
import { useApolloClient, useQuery } from '@apollo/react-hooks'
import styled from 'styled-components'
import Typography from '@material-ui/core/Typography'
import CardContent from '@material-ui/core/CardContent'
import Card from '@material-ui/core/Card'
import { Thermometer } from 'mdi-material-ui'
import { Switch } from '@material-ui/core'
import gql from 'graphql-tag'
import { GET_LATEST_SENSOR_READING } from '../../graphql/queries'

const roundPlaces = (value, places) => {
  const factor = Math.pow(10, places)
  return Math.round(value * factor) / factor
}

const ValueDisplay = ({ className, value }) => {
  return (
    <span className={className}>{value}</span>
  )
}

const StyledValueDisplay = styled(ValueDisplay)`
  font-size: 1.8em;
`

const CardTitle = ({ title, avatar, className }) => {
  return (
    <div className={className}>
      <Thermometer style={{
        height: '50px',
        marginRight: '10px',
        marginLeft: '10px'
      }} />

      <span>{title}</span>
    </div>
  )
}

const TemperatureControl = ({ className, temperature, isMetric }) => {
  if (!temperature) {
    return <Typography className={className}>Temperature not detected</Typography>
  }

  let text = ''
  if (!isMetric) {
    text = `${roundPlaces((temperature * 9 / 5) + 32, 1)}° F`
  } else {
    text = `${temperature}° C`
  }

  return (
    <Typography className={className}>
      Temperature<br />
      <StyledValueDisplay value={text} />
    </Typography>
  )
}

const HumidityControl = ({ className, humidity }) => {
  if (!humidity) {
    return <Typography className={className}>Humidity not detected</Typography>
  }

  return (
    <Typography className={className}>
      Humidity<br />
      <StyledValueDisplay value={`${roundPlaces(humidity, 1)}%`} />
    </Typography>
  )
}

const MetricSwitch = ({ enabled, onChange, className }) => {
  return (
    <div className={className}>
      <label>Use Metric Values</label>
      <Switch checked={enabled} onChange={onChange} />
    </div>
  )
}

const AsOfDate = ({ value, className }) => {
  const ts = new Date(value)
  return <div className={className}><ValueDisplay value={`As of: ${ts.toLocaleString()}`} /></div>
}

const StyledCard = styled(Card)`
  width: 250px;
  height: 250px;  
  margin: 0px 10px;
  display: inline-block;
  
  border: 1px solid rgba(255, 255, 255, 0.3);
  -webkit-background-clip: padding-box; /* for Safari */
  background-clip: padding-box; /* for IE9+, Firefox 4+, Opera, Chrome */
  
  position: relative;
  top: 50%;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
`

const StyledCardHeader = styled(CardTitle)`
  display: flex;
  flex-direction: row;
  align-content: center;
  
  height: 50px;
  line-height: 50px;
  font-size: 1.8em;
  
  box-shadow: 0px 1px 3px #0008;
  background-color: #557f01;
  color: white;
  
  position: relative;
`
const StyledCardContent = styled(CardContent)`
  background-color: #636363;
  color: white;
  height: 100%;
  text-shadow: 0px 0px 5px #000;
`

const StyledTemperature = styled(TemperatureControl)`
  display: inline-block;
  width: 50%;
  text-align: center;
  overflow: hidden;
`

const StyledHumidity = styled(HumidityControl)`
  display: inline-block;
  width: 50%;
  text-align: center;
  overflow: hidden;
`

const StyledMetricSwitch = styled(MetricSwitch)`
  width: 80%;
  font-size: 0.9em;
`

const StyledAsOfDate = styled(AsOfDate)`
  width: 80%;
  font-size: 0.9em;
`

const OptionsContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`

const TemperatureSensorDisplay = ({ sensor }) => {
  const client = useApolloClient()
  const [isMetric, setIsMetric] = useState(false)
  const onIsMetricClick = () => { setIsMetric(!isMetric) }

  const { data: { latestReading }, loading, error } = useQuery(GET_LATEST_SENSOR_READING, {
    variables: {
      name: sensor
    },
    client
  })

  if (loading) {
    return (
      <StyledCard>
        <StyledCardHeader title={sensor} />
        <StyledCardContent>
          <div>Loading</div>
        </StyledCardContent>
      </StyledCard>
    )
  }

  if (error) {
    return (
      <StyledCard>
        <StyledCardHeader title={sensor} />
        <StyledCardContent>
          <div>{error}</div>
        </StyledCardContent>
      </StyledCard>
    )
  }

  console.debug(latestReading, client)
  return (
    <StyledCard>
      <StyledCardHeader title={latestReading.name} />
      <StyledCardContent>
        <StyledTemperature temperature={latestReading.temperature} isMetric={isMetric} />
        <StyledHumidity humidity={latestReading.humidity} />
        <OptionsContainer>
          <StyledMetricSwitch enabled={isMetric} onChange={onIsMetricClick} />
          <StyledAsOfDate value={latestReading.timestamp} />
        </OptionsContainer>
      </StyledCardContent>
    </StyledCard>
  )
}

TemperatureSensorDisplay.propTypes = {
  className: PropTypes.string,
  sensor: PropTypes.string.isRequired
}

HumidityControl.propTypes = {
  className: PropTypes.string,
  humidity: PropTypes.number.isRequired
}

TemperatureControl.propTypes = {
  className: PropTypes.string,
  temperature: PropTypes.number.isRequired,
  isMetric: PropTypes.bool.isRequired
}

MetricSwitch.propTypes = {
  enabled: PropTypes.bool.isRequired,
  onChange: PropTypes.func,
  className: PropTypes.string
}

ValueDisplay.propTypes = {
  className: PropTypes.string,
  value: PropTypes.any
}

AsOfDate.propTypes = {
  value: PropTypes.string,
  className: PropTypes.string
}

CardTitle.propTypes = {
  title: PropTypes.string,
  avatar: PropTypes.string,
  className: PropTypes.string
}

export default TemperatureSensorDisplay
