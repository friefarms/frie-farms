import React from 'react'
import TemperatureSensorDisplay from './TemperatureSensorDisplay'
import EmptySensorList from './EmptySensorList'
import GridList from '@material-ui/core/GridList'
import styled from 'styled-components'

const SENSOR_DISPLAY_AREA_HEIGHT = '315px'

const StyledGridList = styled(GridList)`
  height: ${SENSOR_DISPLAY_AREA_HEIGHT};
`

const TemperatureSensorList = ({ className, sensors }) => {
  if (!sensors || sensors.length < 1) {
    return <EmptySensorList />
  }

  const sensorList = []
  sensors.forEach(s => {
    sensorList.push(<TemperatureSensorDisplay sensor={s} key={s} />)
  })

  if (sensors && sensors.length > 0) {
    return (
      <StyledGridList className={className}>
        {sensorList}
      </StyledGridList>
    )
  }
}

export default styled(TemperatureSensorList)`
  height: ${SENSOR_DISPLAY_AREA_HEIGHT};
`
