#!/bin/bash

cd /home/pi/projects/frie-farms/packages/farm-sensor-d/ || exit
yarn start &

cd /home/pi/projects/frie-farms/packages/farm-sensor-webclient/ || exit
yarn start &

chromium-browser --kiosk http://localhost:3000 &

exit 0
